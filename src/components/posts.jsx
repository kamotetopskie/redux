import React, { Component } from "react";
//import PropTypes from "prop-types";
import { connect } from "react-redux"; //This will connect your component to the redux store
import { fetchPosts } from "../actions/postActions";

class Posts extends Component {
  componentWillMount() {
    this.props.fetchPosts();
  }

  componentWillReceiveProps(nextProps) {
    //When it receives a new property from the state this will run and it takes in a parameter of nextProps
    if (nextProps.newPost) {
      this.props.posts.unshift(nextProps.newPost);
    }
  }

  render() {
    const { posts } = this.props;
    return (
      <div>
        <h1>Posts</h1>
        {posts.map(p => (
          <div key={p.title}>
            <h3>{p.title}</h3>
          </div>
        ))}
      </div>
    );
  }
}

// Posts.PropTypes = {
//   fetchPosts: PropTypes.func.isRequired,
//   posts: PropTypes.array.isRequired,
//   newPost: PropTypes.object
// };

const mapStateToProps = state => ({
  posts: state.posts.items,
  newPost: state.posts.item
});

export default connect(
  mapStateToProps,
  { fetchPosts }
)(Posts);
