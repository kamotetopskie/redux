import { FETCH_POSTS, NEW_POST } from "./types";

export const fetchPosts = () => dispatch => {
  //Thunk middleware allows us to actually call the dispatch function directly so that we can make async requests
  //dispatch is just like a resolver and a promise
  //whenever we want to send a data we need to call the dispatch
  fetch("https://my-json-server.typicode.com/typicode/demo/posts")
    .then(result => result.json()) //This will return promise
    .then(posts =>
      dispatch({
        type: FETCH_POSTS,
        payload: posts
      })
    ); //Here we assign the state to the data that we get from the server.
};

export const createPost = postData => dispatch => {
  console.log(postData);
  fetch("https://my-json-server.typicode.com/typicode/demo/posts", {
    method: "POST", //This we specify the method which is post
    headers: {
      "content-type": "application/json" //We specify the header and we need content type of JSON
    },
    body: JSON.stringify(postData) //Wrap it to the JSON.stringify function to make sure that it is JSON string
  })
    .then(result => result.json()) //Then we get the response. It's not the actual data yet, were just tell it that we want JSON data
    .then(post =>
      dispatch({
        type: NEW_POST,
        payload: post
      })
    ); //Then get the data back
};
